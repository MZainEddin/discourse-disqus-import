require 'nokogiri'
require 'optparse'
require 'csv'
require 'date'
require File.expand_path(File.dirname(__FILE__) + "/base")

class ImportScripts::Disqus < ImportScripts::Base
  def initialize(options)
    verify_file(options[:file])
    verify_file(options[:threads])
    verify_file(options[:users])
    @post_as_user = get_post_as_user(options[:post_as])
    @category_id = options[:category]
    @parser = DisqusSAX.new(options[:strip])
    doc = Nokogiri::XML::SAX::Parser.new(@parser)
    doc.parse_file(options[:file])
    @parser.normalize
    @threads_csv = CSV.parse(File.read(options[:threads]))
    @users_csv = CSV.parse(File.read(options[:users]))
    super()
  end

  def execute
    users = Hash.new
    users_keys = ['name', 'email']
    puts "Creating Users in Forum"
    @users_csv.map {|a| Hash[ users_keys.zip(a) ] }.map do |e|
      if e['email'].present?
        username = UserNameSuggester.suggest(e['name']).chomp('_')
        u = create_user({name: e['name'], email: e['email'], username: username}, nil)
        users[e['name']] = u if u.class == User
        print '.'
      end
    end

    puts "Creating Threads Hash ..................................................."
    threads = Hash.new { |h, k| h[k] = {} }
    threads_keys = ['title', 'slug', 'created_date', 'content']
    @threads_csv.map {|a| Hash[ threads_keys.zip(a) ]}.map{|e| threads[e['slug']] = e}

    notExist  = 0; count = 0; total =  @parser.threads.count
    @parser.threads.each do |id, t|
      count += 1
      puts "Creating #{t[:title]}... (#{t[:posts].size} posts) .................... [ #{count}/#{total} [#{count*100/total}]]"
      slug = t[:link].split('/').last
      if threads.has_key?(slug)
        post_user = @post_as_user
        thread = threads[slug]
        regex = /(?:<p>Submitted By:  )([^<p>]*)(?:<\/p>)/
        if thread['content'].match(regex)
          user_name = thread['content'].match(regex)[1]
          post_user = users[user_name] if user_name.present? && users.has_key?(user_name)
        end
        content = thread['content'].sub regex, ''
        attrs = {
          user_id: post_user.id,
          category: @category_id,
          title: thread['title'],
          slug: thread['slug'],
          raw: content,
          created_at: Date.strptime(thread['created_date'], "%m/%d/%Y")
        }
        post = create_post(attrs, id)
        if post.present?
          print "Creating #{t[:posts].count} post for thread"
          t[:posts].each do |p|
            print "."
            post_user = @post_as_user
            if p[:author_email] && p[:author_name] != 'Anonymous'
              post_user = create_user({ id: nil, email: p[:author_email]}, nil)
            end

            attrs = {
              user_id: post_user.id,
              topic_id: post.topic_id,
              raw: p[:cooked],
              cooked: p[:cooked],
              created_at: Date.parse(p[:created_at])
            }

            if p[:parent_id]
              parent = @parser.posts[p[:parent_id]]

              if parent && parent[:discourse_number]
                attrs[:reply_to_post_number] = parent[:discourse_number]
              end
            end

            post = create_post(attrs, p[:id])
            p[:discourse_number] = post.post_number
          end
          puts
          TopicFeaturedUsers.new(post.topic).choose
        end
      else
        notExist  += 1
        puts "Threads CSV doesn't contain this thread: #{t[:title]} ..............."
      end
    end
    puts "#{notExist.to_s} Threads Not found in the csv"
  end

  private

  def verify_file(file)
    abort("File '#{file}' not found") if !File.exist?(file)
  end

  def get_post_as_user(username)
    user = User.find_by_username_lower(username.downcase)
    abort("No user found named: '#{username}'") if user.nil?
    user
  end
end

class DisqusSAX < Nokogiri::XML::SAX::Document
  attr_accessor :posts, :threads

  def initialize(strip)
    @inside = {}
    @posts = {}
    @threads = {}
    @strip = strip
  end

  def start_element(name, attrs = [])
    case name
    when 'post'
      @post = {}
      @post[:id] = Hash[attrs]['dsq:id'] if @post
    when 'thread'
      id = Hash[attrs]['dsq:id']
      if @post
        thread = @threads[id]
        thread[:posts] << @post
      else
        @thread = {id: id, posts: []}
      end
    when 'parent'
      if @post
        id = Hash[attrs]['dsq:id']
        @post[:parent_id] = id
      end
    end

    @inside[name] = true
  end

  def end_element(name)
    case name
    when 'post'
      @posts[@post[:id]] = @post
      @post = nil
    when 'thread'
      if @post.nil?
        @threads[@thread[:id]] = @thread
        @thread = nil
      end
    end

    @inside[name] = false
  end

  def characters(str)
    record(@post, :author_email, str, 'author', 'email')
    record(@post, :author_name, str, 'author', 'name')
    record(@post, :author_anonymous, str, 'author', 'isAnonymous')
    record(@post, :created_at, str, 'createdAt')

    record(@thread, :link, str, 'link')
    record(@thread, :title, str, 'title')
    record(@thread, :created_at, str, 'createdAt')
  end

  def cdata_block(str)
    record(@post, :cooked, str, 'message')
  end

  def record(target, sym, str, *params)
    return if target.nil?

    if inside?(*params)
      target[sym] ||= ""
      target[sym] << str
    end
  end

  def inside?(*params)
    return !params.find{|p| !@inside[p]}
  end

  def normalize
    @threads.each do |id, t|
      if t[:posts].size == 0
        # Remove any threads that have no posts
        @threads.delete(id)
      else
        # Normalize titles
        t[:title] = [:title].gsub(@strip, '').strip if @strip.present?
      end
    end

    # Merge any threads that have the same title
    existing_title = {}
    @threads.each do |id, t|
      existing = existing_title[t[:title]]
      if existing.nil?
        existing_title[t[:title]] = t
      else
        existing[:posts] << t[:posts]
        existing[:posts].flatten!
        @threads.delete(t[:id])
      end
    end
  end
end

options = {
  post_as: User.first.username,
  category: 1
}

OptionParser.new do |opts|
  opts.banner = 'Usage: RAILS_ENV=production ruby disqus.rb [OPTIONS]'

  opts.on('-f', '--file=FILE_PATH', 'The disqus XML file to import comments from') do |value|
    options[:file] = value
  end

  opts.on('-t', '--threads=FILE_PATH', 'The threads CSV file to import') do |value|
    options[:threads] = value
  end

  opts.on('-u', '--users=FILE_PATH', 'The users CSV file to import') do |value|
    options[:users] = value
  end

  opts.on('-c', '--category=INTEGER', 'The category to insert topics in') do |value|
    options[:category] = value
  end

  opts.on('-p', '--post_as=USERNAME', 'The Discourse username to post as if username not exist') do |value|
    options[:post_as] = value
  end
end.parse!

ImportScripts::Disqus.new(options).perform
