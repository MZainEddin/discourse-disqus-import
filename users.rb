require 'csv'
require File.expand_path(File.dirname(__FILE__) + "/base")

class ImportScripts::Users < ImportScripts::Base
  def initialize(options)
    verify_file(options[:file])
    @users_csv = File.read(options[:file])
    @keys = ['name', 'email']
    super()
  end

  def execute
    CSV.parse(@users_csv).map {|a| Hash[ @keys.zip(a) ] }.map do |e|
      if e['email'].present?
        username = e['name'].gsub("'", "").parameterize.gsub("-", "_")
        username = username[0..User.username_length.end-1].chomp('_') if username.length > User.username_length.end
        create_user({name: e['name'], email: e['email'], username: username}, nil)
      end
    end
  end

  private

  def verify_file(file)
    abort("File '#{file}' not found") if !File.exist?(file)
  end
end

class DisqusSAX < Nokogiri::XML::SAX::Document
  attr_accessor :posts, :threads

  def initialize(strip)
    @inside = {}
    @posts = {}
    @threads = {}
    @strip = strip
  end

  def start_element(name, attrs = [])
    case name
    when 'post'
      @post = {}
      @post[:id] = Hash[attrs]['dsq:id'] if @post
    when 'thread'
      id = Hash[attrs]['dsq:id']
      if @post
        thread = @threads[id]
        thread[:posts] << @post
      else
        @thread = {id: id, posts: []}
      end
    when 'parent'
      if @post
        id = Hash[attrs]['dsq:id']
        @post[:parent_id] = id
      end
    end

    @inside[name] = true
  end

  def end_element(name)
    case name
    when 'post'
      @posts[@post[:id]] = @post
      @post = nil
    when 'thread'
      if @post.nil?
        @threads[@thread[:id]] = @thread
        @thread = nil
      end
    end

    @inside[name] = false
  end

  def characters(str)
    record(@post, :author_email, str, 'author', 'email')
    record(@post, :author_name, str, 'author', 'name')
    record(@post, :author_anonymous, str, 'author', 'isAnonymous')
    record(@post, :created_at, str, 'createdAt')

    record(@thread, :link, str, 'link')
    record(@thread, :title, str, 'title')
    record(@thread, :created_at, str, 'createdAt')
  end

  def cdata_block(str)
    record(@post, :cooked, str, 'message')
  end

  def record(target, sym, str, *params)
    return if target.nil?

    if inside?(*params)
      target[sym] ||= ""
      target[sym] << str
    end
  end

  def inside?(*params)
    return !params.find{|p| !@inside[p]}
  end

  def normalize
    @threads.each do |id, t|
      if t[:posts].size == 0
        # Remove any threads that have no posts
        @threads.delete(id)
      else
        # Normalize titles
        t[:title] = [:title].gsub(@strip, '').strip if @strip.present?
      end
    end

    # Merge any threads that have the same title
    existing_title = {}
    @threads.each do |id, t|
      existing = existing_title[t[:title]]
      if existing.nil?
        existing_title[t[:title]] = t
      else
        existing[:posts] << t[:posts]
        existing[:posts].flatten!
        @threads.delete(t[:id])
      end
    end
  end
end

options = {
  dry_run: false
}

OptionParser.new do |opts|
  opts.banner = 'Usage: RAILS_ENV=production ruby disqus.rb [OPTIONS]'

  opts.on('-f', '--file=FILE_PATH', 'The disqus XML file to import') do |value|
    options[:file] = value
  end

  opts.on('-d', '--dry_run', 'Just output what will be imported rather than doing it') do
    options[:dry_run] = true
  end

  opts.on('-p', '--post_as=USERNAME', 'The Discourse username to post as') do |value|
    options[:post_as] = value
  end

  opts.on('-s', '--strip=TEXT', 'Text to strip from titles') do |value|
    options[:strip] = value
  end
end.parse!

ImportScripts::Users.new(options).perform
